//Soal 1
var flag=0;
console.log("LOOPING PERTAMA");
while(flag<20){
    flag+=2;
    console.log(flag+" - i love coding");
}
console.log("LOOPING KEDUA");
while(flag>0){
    console.log(flag+" - i love coding");
    flag-=2;
}

//Soal 2
for(var i=1;i<21;i++){
    if(i%2!=0 && i%3!=0){ console.log(i+" - Santai"); }
    else if(i%2==0){ console.log(i+" - Berkualitas"); }
    else if(i%2!=0 && i%3==0){ console.log(i+" - I Love Coding"); }
}

//Soal 3
var sharp='';
for(var i=0;i<8;i++){
    for(var j=0;j<i;j++){ sharp+="#"; }
    sharp+="\n";
}
console.log(sharp);

//Soal 4
var kalimat="saya sangat senang belajar javascript";
var kata=kalimat.split(" ");
console.log(kata);

//Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var temp=" ";
for(var i=0;i<5;i++){ 
    for(var j=0;j<5;j++){
        if(daftarBuah[i]<daftarBuah[j]){
            temp=daftarBuah[i];
            daftarBuah[i]=daftarBuah[j];
            daftarBuah[j]=temp;
        }
    }
}
for(var i=0;i<5;i++){ console.log( daftarBuah[i]); }