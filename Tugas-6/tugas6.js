//Soal 1
console.log("------Soal 1------")
const pi=3.14
const luasLingkaran = jariJari => pi*jariJari
const kelilingLingkaran = jariJari => 2*pi*jariJari

console.log("Luas Lingkaran: "+luasLingkaran(3)+" Satuan Luas")
console.log("Keliling Lingkaran: "+kelilingLingkaran(3)+" Satuan Panjang")

//Soal 2
console.log("\n------Soal 2------")
const inputKata = () =>{
  let kata=""
  kata+="saya "
  kata+="adalah "
  kata+="seorang "
  kata+="frontend "
  kata+="developer"
  return kata
}

let kalimat=`${inputKata()}`
console.log(kalimat)

//Soal 3
console.log("\n------Soal 3------")
const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: () => console.log(`${firstName} ${lastName}`)
  }
}
const{fullName} = newFunction("William","Imoh")
fullName()

//Soal 4
console.log("\n------Soal 4------")
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
let {firstName, lastName, destination, occupation,spell}=newObject
console.log(firstName, lastName, destination, occupation, spell)

//Soal 5
console.log("\n------Soal 5------")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined=[...west,...east]
console.log(combined)