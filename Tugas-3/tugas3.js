//Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(kataPertama+" "+kataKedua.substr(0,1).toUpperCase()+
            kataKedua.substr(1,5)+" "+kataKetiga+" "+kataKeempat.toUpperCase());


//Soal 2
var angkaPertama = "1";
var angkaKedua = "2";
var angkaKetiga = "4";
var angkaKeempat = "5";

console.log(Number(angkaPertama)+Number(angkaKedua)+
            Number(angkaKetiga)+Number(angkaKeempat));

//Soal 3
var kalimat = 'wah javascript itu keren sekali';

var firstWord = kalimat.substring(0,3);
var secondWord = kalimat.substring(4,14);
var thirdWord = kalimat.substring(15,18);
var fourthWord = kalimat.substring(19,24);
var fiveWord = kalimat.substring(25,31);

console.log('Kata Pertama: ' + firstWord);
console.log('Kata Kedua: ' + secondWord);
console.log('Kata Ketiga: ' + thirdWord);
console.log('Kata Keempat: ' + fourthWord);
console.log('Kata Kelima: ' + fiveWord);

//Soal 4
var nilai=76;

if(nilai>=80 && nilai<=100){
    console.log("A");
} else if(nilai>=70 && nilai<80){
    console.log("B");
} else if(nilai>=60 && nilai<70){
    console.log("C");
} else if(nilai>=50 && nilai<60){
    console.log("D");
} else if(nilai<50 && nilai>=0){
    console.log("E");
}

//Soal 5
var tanggal = 11;
var bulan = 2;
var tahun = 1999;

switch(bulan){
    case 1: console.log(tanggal+" Januari "+tahun); 
        break;
    case 2: console.log(tanggal+" Februari "+tahun);
        break;
    case 3: console.log(tanggal+" Maret "+tahun);
        break;
    case 4: console.log(tanggal+" April "+tahun);
        break;
    case 5: console.log(tanggal+" Mei "+tahun);
        break;
    case 6: console.log(tanggal+" Juni "+tahun);
        break;
    case 7: console.log(tanggal+" Juli "+tahun);
        break;
    case 8: console.log(tanggal+" Agustus "+tahun);
        break;
    case 9: console.log(tanggal+" September "+tahun);
        break;
    case 10: console.log(tanggal+" Oktober "+tahun);
        break;
    case 11: console.log(tanggal+" November "+tahun);
        break;
    case 12: console.log(tanggal+" Desember "+tahun);
        break;
    default: console.log("Tidak ada bulan tersebut.");
        break;
}