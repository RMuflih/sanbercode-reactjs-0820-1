//Soal 1
console.log("------Soal 1------")
class Animal{
    constructor(name){
        this._animalname=name
        this._legs=4
        this._cold_blooded=false
    }
    get name(){
        return this._animalname
    }
    get legs(){
        return this._legs
    }
    get cold_blooded(){
        return this._cold_blooded
    }
    set name(x){
        this._animalname=x
    }
}

var sheep = new Animal("shaun")

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

class Frog extends Animal{
    constructor(name){
        super(name)
    }
    jump(){
        console.log(`hop hop`)
    }
}
class Ape extends Animal{
    constructor(name){
        super(name)
        this._legs=2
    }
    get legs(){
        return this._legs
    }
    yell(){
        console.log(`Auooo`)
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell()
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)

var kodok = new Frog("buduk")
kodok.jump()
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)


//Soal 2
console.log("\n------Soal 2------")
class Clock{
    constructor({template}){
        this.template = template
    }
    render(){
        var date = new Date()

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs)

        console.log(output);    
    }
    stop(){
        clearInterval(this.timer)
    }
    start(){
        this.render()
        this.timer=setInterval(this.render.bind(this),1000)
    }
}
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start();