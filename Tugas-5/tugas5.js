//Soal 1
console.log("------Soal 1------");
function halo(){ return "Halo Sanbers!"; }

console.log(halo());

//Soal 2
console.log("\n------Soal 2------");
function kalikan(angkaPertama, angkaKedua){ return angkaPertama*angkaKedua; }

var num1=12;
var num2=4;

var hasilKali=kalikan(num1,num2);
console.log(hasilKali);

//Soal 3
console.log("\n------Soal 3------");
function introduce(name, age, address, hobby){
    return "Nama Saya "+name+", umur saya "+age+
           " tahun, "+"alamat saya di "+address+
           ", dan saya punya hobby yaitu "+hobby+"!";
}

var name="John";
var age=30;
var address="Jalan belum jadi";
var hobby="Gaming";

var perkenalan=introduce(name,age,address,hobby);
console.log(perkenalan);

//Soal 4
console.log("\n------Soal 4------");
var arrayDaftarPeserta={
    nama:"Asep",
    jenisKelamin:"laki-laki",
    hobi:"baca buku",
    tahunLahir:1992
}

console.log(arrayDaftarPeserta);

//Soal 5
console.log("\n------Soal 5------");
var buah=[
    {nama:"strawberry",warna:"merah",adaBijinya:false,harga:9000},
    {nama:"jeruk",warna:"oranye",adaBijinya:true,harga:8000},
    {nama:"Semangka",warna:"Hijau & Merah",adaBijinya:true,harga:10000},
    {nama:"Pisang",warna:"Kuning",adaBijinya:false,harga:5000}
];

console.log(buah[0]);

//Soal 6
console.log("\n------Soal 6------");
var dataFilm=[];

function inputFilm(){
    dataFilm.push(
        {nama:"Bloodshot",
         durasi:"1h 49m",
         genre:"Action",
         tahun:2020},
        {nama:"Brightburn",
         durasi:"1h 30m",
         genre:"Horror",
         tahun:2019}
    )
    return dataFilm;
}

var tampilFilm=inputFilm();
console.log(tampilFilm);